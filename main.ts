import {
    GetProductsForIngredient,
    GetRecipes
} from "./supporting-files/data-access";
import { Product, RecipeLineItem, UoMType, UoMName, UnitOfMeasure } from "./supporting-files/models";
import { GetBaseUoM, NutrientBaseUoM } from "./supporting-files/data-access";
import {
    GetCostPerBaseUnit,
    ConvertUnits,
    SumUnitsOfMeasure,
    GetNutrientFactInBaseUnits
} from "./supporting-files/helpers";
import { RunTest, ExpectedRecipeSummary } from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

interface SelectedProduct {
	lineItem: RecipeLineItem;
	totalCost: number;
	product: Product | null;
}

interface ProductNutrient {
	nutrientName: string;
	quantityAmount: UnitOfMeasure;
}

function GetIngredientInBaseUoM(base: UoMType, ingredientUoM: UnitOfMeasure) {
	const baseUnitOfMeasure = GetBaseUoM(
	       base
	    );

		// No conversion for cups to grams in ConvertUnits, convert to cup -> ml, ml -> grams
		if(ingredientUoM.uomName === UoMName.cups && baseUnitOfMeasure.uomName === UoMName.grams ) {
			const inML = ConvertUnits(
		        ingredientUoM,
		        UoMName.millilitres,
		        UoMType.volume
		    );

			return ConvertUnits(
	        inML,
	        baseUnitOfMeasure.uomName,
	        baseUnitOfMeasure.uomType
	    );
		}

		return ConvertUnits(
	        ingredientUoM,
	        baseUnitOfMeasure.uomName,
	        baseUnitOfMeasure.uomType
	    );

}

// Total in ExpectedRecipeSummary exceeds cheapest cost for cream
// Show cheapest cost for cream
const creamBrulee = recipeData[0];
const cream = creamBrulee.lineItems[0]

const creamProducts = GetProductsForIngredient(cream.ingredient)
const creamInML = ConvertUnits(cream.unitOfMeasure, UoMName.millilitres, UoMType.volume)
console.log(`Ingredient: ${cream.ingredient.ingredientName} ${creamInML.uomAmount} ${creamInML.uomName} \n`)

let creamSuppliers: any = [];
creamProducts.map(creamProduct => {
	creamSuppliers = [...creamSuppliers, ...creamProduct.supplierProducts]
})

creamSuppliers.map((creamSupplier: any) => {
	const costPer100ML = (creamSupplier.supplierPrice/creamSupplier.supplierProductUoM.uomAmount)*100
	console.log(`${creamSupplier.supplierName} - ${creamSupplier.supplierProductName}\nPRICE: ${creamSupplier.supplierPrice} @ ${creamSupplier.supplierProductUoM.uomAmount} ${creamSupplier.supplierProductUoM.uomName} - ${costPer100ML} @ 100 ${creamSupplier.supplierProductUoM.uomName}\nCOST at ${creamInML.uomAmount} ${creamInML.uomName} - ${(creamInML.uomAmount/100)*costPer100ML}
		 \n`)
		
})


recipeData.map(recipe => {
	let totalUoM: UnitOfMeasure ={uomAmount: 0, uomName: UoMName.grams, uomType: UoMType.mass} // in grams

	// Get selected (cheapest) product for each recipe ingredient
	const selectedProducts: SelectedProduct[] = recipe.lineItems.map(lineItem => {		
		const initial: SelectedProduct = {
			lineItem,
			totalCost: 0,
			product: null,
		}		 
	   
		const products = GetProductsForIngredient(lineItem.ingredient)
		const selectedProduct: SelectedProduct = products.reduce((current, product) => {
			let costForRecipe = current.totalCost;
			let cheapestProduct = current.product || product;
			const suppliers = product.supplierProducts;
			suppliers.map(supplier => {			
				const costPerBaseUnit = GetCostPerBaseUnit(supplier)
				const ingredientInBaseUoM = GetIngredientInBaseUoM(supplier.supplierProductUoM.uomType, lineItem.unitOfMeasure)
			   
				const productCostForRecipe = ingredientInBaseUoM.uomAmount*costPerBaseUnit;

				// Use first product cost if there is no total cost in the current value yet
				costForRecipe = costForRecipe  || productCostForRecipe;
				
				// Get cheapest cost and product
				if(productCostForRecipe < costForRecipe) {
					costForRecipe =  productCostForRecipe;
					cheapestProduct = product;
				}
			})

			return {
				lineItem,
				totalCost: costForRecipe,
				product: cheapestProduct,
			}
		}, initial)
		return selectedProduct;
	})

	// Get recipe nutrients
	let productNutrients: ProductNutrient[] = []

	selectedProducts.map(selected => {
		const inGrams = GetIngredientInBaseUoM(UoMType.mass, selected.lineItem.unitOfMeasure)
		totalUoM = SumUnitsOfMeasure(totalUoM, inGrams)
		selected.product?.nutrientFacts.map(fact => {
			const productNutrient = GetNutrientFactInBaseUnits(fact)
			const totalInRecipe = (inGrams.uomAmount/productNutrient.quantityPer.uomAmount)*productNutrient.quantityAmount.uomAmount
			productNutrients.push({
				nutrientName: fact.nutrientName,
				quantityAmount: {uomAmount: totalInRecipe, uomName: UoMName.grams, uomType: UoMType.mass}
			})
		})
	})

	let recipeNutrients: any = {}

	productNutrients.map(nutrient => {
		if(!recipeNutrients[nutrient.nutrientName]) {
			recipeNutrients[nutrient.nutrientName] = {...nutrient}
			return;
		}
		recipeNutrients[nutrient.nutrientName] = {
			...recipeNutrients[nutrient.nutrientName], 
			quantityAmount: SumUnitsOfMeasure(recipeNutrients[nutrient.nutrientName]['quantityAmount'], nutrient.quantityAmount)
		} 
	})

	 Object.keys(recipeNutrients).map(recipeNutrient => {
		recipeNutrients[recipeNutrient] = {
			...recipeNutrients[recipeNutrient],
			quantityAmount: {
				// Calculate amount per 100g in the recipe
				uomAmount: (recipeNutrients[recipeNutrient].quantityAmount.uomAmount/totalUoM.uomAmount)*NutrientBaseUoM.uomAmount,
				uomName: UoMName.grams, uomType: 
				UoMType.mass
			},
			quantityPer: {
				uomAmount: NutrientBaseUoM.uomAmount,
				uomName: UoMName.grams, 
				uomType: UoMType.mass
			}
		}
	})
		
	const recipeTotalCost = selectedProducts.reduce((total,selectedProduct) => total + selectedProduct.totalCost, 0)

	recipeSummary[recipe.recipeName] = {
		cheapestCost: recipeTotalCost,
        nutrientsAtCheapestCost: recipeNutrients
	}
})

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
